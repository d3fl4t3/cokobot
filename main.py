import requests, re, telebot, os, time
from config import *

# debug
if DEBUG:
    import http.client

    http.client.HTTPConnection.debuglevel = 1


def get_results():
    raw = requests.get("http://coko.tomsk.ru/exam2017/default.aspx").text
    strs = re.findall("<nobr>([^<]+)</nobr>", raw)
    if not strs:
        raise Exception("Site doesn't work")
    return dict(zip(strs[::5], strs[2::5]))


def get_new_results():
    results = get_results()
    results = {k: v for k, v in results.items() if v != "0"}  # filter only available results
    with open(RESULTS_FILE) as old_results:
        return set(results).difference(set(old_results.read().strip().split("\n")))


def main():
    bot = telebot.TeleBot("")
    bot.config["api_key"] = BOT_TOKEN
    try:
        results = get_new_results()
        if results:
            bot.send_message(CHANNEL_NAME, CHANNEL_MESSAGE(results))
            with open(RESULTS_FILE, "a") as f:
                f.write('\n'.join(results))
                f.write('\n')
    except Exception as e:
        print(e)


if __name__ == "__main__":
    if not os.path.exists(RESULTS_FILE):
        with open(RESULTS_FILE, "w") as f:
            pass
    main()
    time.sleep(10)  # trying not to ddos their site :)
